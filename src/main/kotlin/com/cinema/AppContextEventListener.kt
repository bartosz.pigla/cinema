package com.cinema

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.core.env.ConfigurableEnvironment
import org.springframework.core.env.EnumerablePropertySource
import org.springframework.stereotype.Component

@Component
class AppContextEventListener {
    private val logger: Logger = LoggerFactory.getLogger(AppContextEventListener::class.java)

    @EventListener
    fun handleContextRefreshed(event: ContextRefreshedEvent) {
        printActiveProperties(event.applicationContext.environment as ConfigurableEnvironment)
    }

    fun printActiveProperties(env: ConfigurableEnvironment) {
        println("************************* ACTIVE APP PROPERTIES ******************************")
        env.propertySources
            .asSequence()
            .filter { it.name.contains("applicationConfig") }
            .map { it as EnumerablePropertySource<*> }
            .map { it -> it.propertyNames.toList() }
            .flatten()
            .distinctBy { it }
            .sortedBy { it }
            .toList()
            .forEach { it ->
                try {
                    println("$it=${env.getProperty(it)}")
                } catch (e: Exception) {
                    logger.warn("$it -> ${e.message}")
                }
            }
        println("******************************************************************************")
    }
}
